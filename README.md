# AlgorythmID3 - PD

En este trabajo vamos a tratar el algoritmo ID3, su uso se engloba en la búsqueda 
de hipótesis o reglas, dado un conjunto de ejemplos (o conjunto de entrenamiento).
En este momento tenemos un archivo “Main”, el cual contiene nuevo tipo que nos 
hemos creados y que van a ser muy útiles a la hora de desarrollar este trabajo.

También podrás encontrar algunos métodos como pueden ser:
-	entropía: Calcula la entropia de una lista de valores.

-	splitAtributo: Separa las características de cada atributo.

-	splitEntropia: Obtiene  cada una de las  entropías cuando separa un atributo por sus características.

Para nuestro caso vamos a usar el paquete que contiene el estándar Haskell Prelude
y sus bibliotecas de soporte, y una gran colección de bibliotecas útiles que van
desde estructuras de datos hasta combinadores de análisis y utilidades de depuración.

Además, también nos apoyaremos en algunas librerías propias de scikit Learn que
creamos que pueden ser útil para el desarrollo de este trabajo.

Con respecto al conjunto de entrenamiento, hemos elegido uno de
“UCI Machine Learning Repository”, cuyo enlace puedes encontrar el apartado de
referencias (punto 4 de este artículo). Hemos escogido el conjunto de entrenamiento
“Iris Data Set” que consta de 4 atributos y 1 atributo de clasificación.
Los atributos son sepal length, sepal width, petal lentgh y petal width
(anchura y altura de sépalo, y altura y anchura de pétalo).
El atributo de clasificación consta de tres valores: setosa, versicolour y virginica.