-- Importamos las librerias necesarias
import Data.List (nub, elemIndices)
import qualified Data.Map as M
import Data.Map (Map, (!))
import Data.List (transpose)
import Text.CSV

-- Definomos algunos tipos necesarios
type Clases = String
type Atributos = String
type Entropia = Double
type DataSet = [([String], Clases)]

-- Método para calcular la entropia de una lista de valores
entropia :: (Eq a) => [a] -> Entropia

entropia xs = sum $ map (\x -> prob x * info x) $ nub xs
   where prob x  = (length' (elemIndices x xs)) / (length' xs)
         infor x = negate $ logBase 2 (prob x)
		 length' xs = fromIntegral $ length xs
		 
		 
-- Método para separar las características de cada atributo
splitAtributo :: [(Atributos, Clases)] -> Map Atributos [Clases]

splitAtributo fc = foldl (\m (f,c) -> M.insertWith (++) f [c] m) M.empty fc


-- Método para obtener las entropias de los atributos divididos por sus características
splitEntropia :: Map Atributos [Clases] -> M.Map Atributos Entropia

splitEntropia m = M.map entropia m